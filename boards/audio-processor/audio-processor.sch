EESchema Schematic File Version 5
EELAYER 36 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 15
Title "Electric Violin Audio Processor"
Date ""
Rev ""
Comp "ECE445 Team 22"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 4500 3550
Connection ~ 4550 3450
Connection ~ 4600 3350
Connection ~ 4650 3250
Wire Wire Line
	2850 2600 2850 3000
Wire Wire Line
	2850 3000 4500 3000
Wire Wire Line
	3400 2600 3400 2900
Wire Wire Line
	3400 2900 4550 2900
Wire Wire Line
	3800 2850 3800 2600
Wire Wire Line
	4350 2800 4350 2600
Wire Wire Line
	4350 3250 4650 3250
Wire Wire Line
	4350 3350 4600 3350
Wire Wire Line
	4350 3450 4550 3450
Wire Wire Line
	4350 3550 4500 3550
Wire Wire Line
	4350 4200 4800 4200
Wire Wire Line
	4350 4400 4800 4400
Wire Wire Line
	4350 4500 4400 4500
Wire Wire Line
	4400 4500 4400 4700
Wire Wire Line
	4500 3000 4500 3550
Wire Wire Line
	4500 3550 4800 3550
Wire Wire Line
	4550 2900 4550 3450
Wire Wire Line
	4550 3450 4800 3450
Wire Wire Line
	4600 2850 3800 2850
Wire Wire Line
	4600 3350 4600 2850
Wire Wire Line
	4600 3350 4800 3350
Wire Wire Line
	4650 2800 4350 2800
Wire Wire Line
	4650 3250 4650 2800
Wire Wire Line
	4650 3250 4800 3250
Wire Wire Line
	4800 4100 4350 4100
Wire Wire Line
	4800 4300 4350 4300
Wire Wire Line
	6250 4200 6250 4350
Wire Wire Line
	6300 4100 5850 4100
Wire Wire Line
	6300 4200 6250 4200
Text Notes 4250 4700 2    50   ~ 0
Offboard piezo connector(s)\n
Text Notes 6450 4350 0    50   ~ 0
Offboard TS connector
$Comp
L Connector:TestPoint TP?
U 1 1 62258540
P 2850 2600
AR Path="/6206DF1F/62258540" Ref="TP?"  Part="1" 
AR Path="/62258540" Ref="TP7"  Part="1" 
F 0 "TP7" H 2908 2718 50  0000 L CNN
F 1 "CS_DSP" H 2908 2627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 3050 2600 50  0001 C CNN
F 3 "~" H 3050 2600 50  0001 C CNN
	1    2850 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 62256031
P 3400 2600
AR Path="/6206DF1F/62256031" Ref="TP?"  Part="1" 
AR Path="/62256031" Ref="TP8"  Part="1" 
F 0 "TP8" H 3458 2718 50  0000 L CNN
F 1 "SCK" H 3458 2627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 3600 2600 50  0001 C CNN
F 3 "~" H 3600 2600 50  0001 C CNN
	1    3400 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 62256025
P 3800 2600
AR Path="/6206DF1F/62256025" Ref="TP?"  Part="1" 
AR Path="/62256025" Ref="TP9"  Part="1" 
F 0 "TP9" H 3858 2718 50  0000 L CNN
F 1 "POCI" H 3858 2627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 4000 2600 50  0001 C CNN
F 3 "~" H 4000 2600 50  0001 C CNN
	1    3800 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 6225602B
P 4350 2600
AR Path="/6206DF1F/6225602B" Ref="TP?"  Part="1" 
AR Path="/6225602B" Ref="TP10"  Part="1" 
F 0 "TP10" H 4408 2718 50  0000 L CNN
F 1 "PICO" H 4408 2627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 4550 2600 50  0001 C CNN
F 3 "~" H 4550 2600 50  0001 C CNN
	1    4350 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 624FF473
P 4400 4700
AR Path="/6206DF42/624FF473" Ref="#PWR?"  Part="1" 
AR Path="/624FF473" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 4400 4450 50  0001 C CNN
F 1 "GND" H 4405 4527 50  0000 C CNN
F 2 "" H 4400 4700 50  0001 C CNN
F 3 "" H 4400 4700 50  0001 C CNN
	1    4400 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 62502EA3
P 6250 4350
AR Path="/6206DF42/62502EA3" Ref="#PWR?"  Part="1" 
AR Path="/62502EA3" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 6250 4100 50  0001 C CNN
F 1 "GND" H 6255 4177 50  0000 C CNN
F 2 "" H 6250 4350 50  0001 C CNN
F 3 "" H 6250 4350 50  0001 C CNN
	1    6250 4350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 622B5FE3
P 6450 2550
F 0 "H1" H 6550 2596 50  0000 L CNN
F 1 "MountingHole" H 6550 2505 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6450 2550 50  0001 C CNN
F 3 "~" H 6450 2550 50  0001 C CNN
	1    6450 2550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 622B7103
P 6450 2850
F 0 "H2" H 6550 2896 50  0000 L CNN
F 1 "MountingHole" H 6550 2805 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6450 2850 50  0001 C CNN
F 3 "~" H 6450 2850 50  0001 C CNN
	1    6450 2850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 622BC24A
P 6500 4100
F 0 "J4" H 6580 4092 50  0000 L CNN
F 1 "Conn_01x02" H 6580 4001 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6500 4100 50  0001 C CNN
F 3 "~" H 6500 4100 50  0001 C CNN
	1    6500 4100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x05 J3
U 1 1 622DE98D
P 4150 4300
F 0 "J3" H 4068 3875 50  0000 C CNN
F 1 "Conn_01x05" H 4068 3966 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-05A_1x05_P2.54mm_Vertical" H 4150 4300 50  0001 C CNN
F 3 "~" H 4150 4300 50  0001 C CNN
	1    4150 4300
	-1   0    0    1   
$EndComp
$Sheet
S 4800 3150 1050 1500
U 6206DF42
F0 "Audio and DSP" 50
F1 "audio_dsp.sch" 50
F2 "SDI" I L 4800 3250 50 
F3 "SCK" I L 4800 3450 50 
F4 "SDO" O L 4800 3350 50 
F5 "~{CS}" I L 4800 3550 50 
F6 "PIEZO1" I L 4800 4100 50 
F7 "PIEZO2" I L 4800 4200 50 
F8 "PIEZO3" I L 4800 4300 50 
F9 "PIEZO4" I L 4800 4400 50 
F10 "INSTR_OUT" O R 5850 4100 50 
$EndSheet
$Sheet
S 4850 2400 950  450 
U 6206DF01
F0 "Power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 3550 3150 800  600 
U 6206DF1F
F0 "User Interface" 50
F1 "interface.sch" 50
F2 "CS_DSP" O R 4350 3550 50 
F3 "SDO" O R 4350 3250 50 
F4 "SCK" O R 4350 3450 50 
F5 "SDI" I R 4350 3350 50 
F6 "CLKOUT" O R 4350 3650 50 
$EndSheet
$EndSCHEMATC
