# Film capacitors

Here is a video talking about using film caps in an audio application. [https://www.youtube.com/watch?v=TRzSJFI0HMI](https://www.youtube.com/watch?v=TRzSJFI0HMI)

Most of the other sources I could find suggest that the film caps "sound good", and Prof. Haken says they "age well" as well. I'm not sure what either of these mean quantitatively, but the video talks about the ESR of film caps (vs. electrolytics), so perhaps it is related to this parameter.

The same channel has a video describing the sound differences between capacitors: [https://www.youtube.com/watch?v=mCk50RTtrT0](https://www.youtube.com/watch?v=mCk50RTtrT0)
