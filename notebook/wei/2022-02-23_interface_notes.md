### User Interface Update

Feb 23, 2022

After tabulating the component costs for the Feb. 22 user interface design, I found that the total cost was $55.71, of which $19.56 was from the quadrature clock converters. This accounts for 35% of the total cost of parts for this portion of our design.

The LS7183 does not do much that we couldn't program ourselves directly on the interface microcontroller. With 128K of program memory, fitting interrupt handlers to count encoder ticks should not be a problem on the MKL43Z128. Also, the LS7183 is less available than all the other parts, since it is made available through Digikey Marketplace and requires 2 weeks extra to ship.

As a result of this analysis, I have removed the LS7183 from the design, along with its supporting hardware.
