Feb 10, 2022

Note for implementing on hardware.
Take a look at the difference equation for the bandpass filter (taken from RBJ cookbook).

`y[n] = (b0/a0)*x[n] + (b1/a0)*x[n-1] + (b2/a0)*x[n-2] - (a1/a0)*y[n-1] - (a2/a0)*y[n-2]`

What do we do to calculate y[0] and y[1], when we don't have some of the "past" samples?


We could maintain a frame buffer with this kind of numbering:
`n -2 -1  0  1 ... 512(or however many)`

All samples start out at 0. As new data is shifted in, some of it goes into the "-2" and "-1" places, which can be used to calculate y[0] in the current frame of audio data using the actual past data. This should protect against audio artefacts at the buffer boundaries that might occur if we zero those terms.
