# Rotary encoder schematic
Feb 13, 2022

The following is a schematic for a rotary encoder, from [https://swharden.com/blog/2017-02-02-rotary-encoder/](https://swharden.com/blog/2017-02-02-rotary-encoder/)

![Rotary encoder schematic (with button)](encoder_schematic.jpg)

I used this circuit in our own design. RBIAS should be chosen to provide around 25 microseconds of pulse width, which puts its value around 2.2-2.7kOhm according to Fig. 5 in the LS7183N datasheet. The zero-ohm resistors are a failsafe for us - if we cannot get the LS7183N in time, we may bypass its part of the circuit and work directly with the encoder clock signals.

![Rotary encoder and quadrature decoder circuit](encoder_decoder_circuit.png)
![Figure 5 of the LS7183N datasheet](figure_5_ls7183n.png)
