Feb 11, 2022

Requirements for interface microcontroller:
- SPI connectivity
- Support for off-chip flash/EEPROM, or internal EEPROM. 
    - Note that EEPROM will wear out after a certain number of write cycles faster than flash, so if we're just saving four numbers per preset EEPROM is wasteful.

Helpful:
- Easy to program, with minimal bare-metal code to write.

I am familiar with Mbed OS and MCUXpresso SDK, so if we use this then the development is likely to be less painful than if we tried to write bare-metal code for the interface. NXP actually pushes MCUXpresso for their products, while Mbed OS is kind of an extra (like Arduino).

Initial thoughts:
MKL13Z32VLH4 - SPI connectivity, but no EEPROM. MCUXpresso compatible. Actually in stock (as of Feb 11) at Digikey.
