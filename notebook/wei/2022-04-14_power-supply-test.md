# Overcurrent protection test

Test Resistance: 5.682 ohms

Voltage at 3V3 normally: 3.31698 V

Resistance placed between ground and left pad of F1 (read such that "F1" refdes is right-side-up).

Voltage at 3V3 when VBAT is shorted to ground through test resistance: 3V3

Current measured at power supply: 1.037 A. This is within the range defined as "overcurrent" as stated in the Design Document.

This result is not as expected. Over 1 A of current is flowing through the PTC fuse, but the rest of the power supply does not shut off. This suggests that either the overcurrent protection circuit does not work as stated, or that the test itself is flawed.

# Reverse polarity protection test

Voltage measured at 3V3: -0.7061 V

Voltage measured at 5V: -0.977 V

Steady-state current from power supply: 0.203 A

We did not define a particular voltage range as acceptable for reverse-polarity protection. Since both 5V and 3V3 are within 1 V of ground potential, it is likely that the components will be protected. However, I cannot say for sure whether the negative voltage will damage the power supplies of the microcontroller and DSP.

While preparing the following test, I accidentally left the power supply configured in reverse polarity (6.000 V and 1.000 A limit). The testing board did not turn on. When I corrected the polarity, the board turned on and appears to work as normal. It is therefore very likely that the reverse polarity protection is working.

# All LEDs on test

When all lightbar LEDs, one preset LED, one string LED (and the power light) are on, the current drawn from power supply is 0.191 A. This falls well below the range of 300 ± 15 mA stated in the Design Document. Further testing is needed once the DSP is active.

# Addendum on 2022-04-22

During the assembly process, there were several short circuits in the DSP pins and 3V3 regulators due to imprecise assembly. The board did not turn on when it was powered on. However, once the short circuits were removed, the board turned on and was able to be programmed. This is circumstantial evidence that the OCP circuit is working to protect the rest of the subsystems from short circuits, justnot at the 1A cutoff stated in the Design Document.
