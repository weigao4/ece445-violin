I made a quick implementation of the bandpass filter from the RBJ Audio EQ Cookbook. The settings are Fs = 44100 Hz, f0 = 440 Hz, Q = 20.

The frequency response and the effect the filter has on a chirp (frequency sweep) from 0 to 22050 Hz (Nyquist frequency) is shown here.

![Frequency response](rbj_bandpass_freqz.png)
![Chirp filtered](rbj_bandpass_chirp.png)


 
