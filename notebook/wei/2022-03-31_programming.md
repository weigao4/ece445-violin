# Programming the MKL43Z128 with Mbed Studio

In order to use Mbed Studio to program the user interface microcontroller, the linker script must be modified to use the memory map of the MKL43Z128. The provided linker scripts are for the MKL43Z256, which has twice as much program flash and RAM.

The linker scripts are in `mbed-os/targets/TARGET_Freescale/TARGET_MCUXpresso_MCUS/TARGET_KL43Z/device/`. There is one linker script for each of the IAR, gcc/ARM, and ARM-STD toolchains (.sct, .ld, or .icf). The format of each script is slightly different, but the relevant memory values are as follows.

| Flash start | Flash end | RAM start | RAM end |
| 0x00000000  | 0x0001ffff | 0x1ffff000 | 0x20002ffff | 

(To see how these values are calculated, consult Chapter 4, _Memory Map_, of the KL43 Sub-Family Reference Manual.)


Ideally we would define a new target, instead of hacking the old one like this.
