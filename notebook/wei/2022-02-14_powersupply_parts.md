# Power Supply Parts

Feb 14, 2022

This is a tentative list of parts for the power supply.

# Parts

| Part name | Part number | Unit price | Quantity |
| --------- | ----------- | ---------- | -------- |
| 5V LDO regulator | NCP1117IDT50T4G | 0.67 | 1 |
| 3.3V regulator | XC6227C331PR-G | 0.95 | 1 |
| Battery connector | - | - | - |
| Regulator capacitors (0.1uF) | - | - | 4 |
| 5V capacitor (10uF) | 106BPS050M | 0.42 | 1 |
| 3.3V capacitor (10uF) | 106BPS050M | 0.42 | 1 |
| Reverse polarity protection PFET | DMP2066LSN-7 | 0.51 | 1 |
| Amber LED (power) | IN-S42BT5A | 0.32 | 1 |
| Power LED resistor | ERJ-3GEYJ271V | 0.10 | 1 |
| 1A resettable fuse | | | 1 |


# Notes

The PFET for reverse polarity protection is massively derated. Its maximum drain-gate voltage is 60V and max. gate-source voltage is ±25V, much more than the 6V nominal battery voltage. As of 2022-02-15, there is only 1 of these in stock at Digikey - alternative FETs with similar characteristics may be used instead.

See the [user interface parts list](2022-02-13_interface_parts.md) for the power analysis of the LEDs. To be safe, I have picked a 3.3V regulator rated for 700mA. If the digital section of the design somehow drew 700mA, this would leave 300mA for the analog/audio section of the design. A more detailed analysis is needed once the analog section is designed.

The power smoothing caps are through-hole components.

# Changelog

### 2022-02-14

- Initial draft
- Remarked on reverse polarity protection

### 2022-02-15

- Replaced 3.3V regulator with higher current part
- Moved one LED/resistor from the interface parts list
- Added part number for power smoothing caps
- Added note about alternative PFETs
- Switched to smaller DMP2066LSN-7 PFET

### 2022-02-20

- Added resettable fuse

