# DSP clock source

The DSP can either use a crystal oscillator or take a clock input from an external source. The maximum clock speed is about 18 MHz and the minimum is about 11 MHz. 

It is not practical to route a fast clock trace from the user interface to the DSP since the distance is too great. We have specified a 16 MHz crystal instead.


![dsp crystal circuit](dsp-crystal.png)

The particular crystal is NX5032GA-16MHZ-STD-CSK-8. The load capacitance is 8 pF.
