# Formant filtering

February 14, 2022

## Overview

The power of these biquad bandpass filters lies in using a bank of 3-4 them to shape the harmonics of a harmonic-rich audio signal. One use of a biquad filter bank like this is to set the center frequences to the formant (harmonic) frequencies of certain vowels. This causes the processed sound to sound as if something is "saying" that vowel, so we could have our violin saying "AAAA" on the G string and "EEEE" on the E string.

## Formant frequencies


# Citations
