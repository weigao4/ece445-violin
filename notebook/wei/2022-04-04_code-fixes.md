# First round of code debugging

Turns out that hardware SPI on the KL43 series (and probably other microcontrollers) can only be used on specific pins, and we just happened to pick the wrong pins in both the first and second PCB revisions. To compensate, we are using the [SWSPI library](https://os.mbed.com/users/davervw/code/SWSPI/), which allows for SPI to be used on arbitrary GPIO pins.

The testing setup is a timer-scheduler loop which blinks a light every 0.1 seconds.

When the code for the lightbars is included in the build, the blinking doesn't work. This may be due to invalid memory accesses caused by the way that the MCP23S08 peripherals were originally initialized:

```c++
void LightBar::begin(SWSPI * spi, DigitalOut * cs, const unsigned int addr)
{
    *_mcp = mcp23s08(addr);
    _mcp->begin(spi, cs);
    _mcp->setIODirection(0x00); // set all IOs as Output
}
```

In particular, `_mcp` is a pointer to a `mcp23s08` object. 
`_mcp` is dereferenced and assigned to using a newly-constructed `mcp23s08`. 
But the space for this object has not yet actually been allocated under `_mcp`! 
The pointer is invalid. So I changed this to

```c++
LightBar::LightBar(const unsigned int addr) : _mcp(mcp23s08(addr))
{
}
void LightBar::begin(SWSPI * spi, DigitalOut * cs)
{
   // Any reason we can't roll this into the constructor?
    _mcp.begin(spi, cs);
    _mcp.setIODirection(0x00); // set all IOs as Output
}
```

(plus relevant changes in the header). We'll see if this actually fixes the problem.

I also replaced the Arduino-style pin names (D0, D1, etc.) with their actual port-pin names (PTA0, PTE20, etc.)

