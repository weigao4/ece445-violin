The first task is to determine how to implement a bandpass filter in the digital domain. Once we have this figured out then porting it to the hardware will be easier.

As I recall from ECE420, calculating filter coefficients can be done based on the spectral response, or by using standard methods like Butterworth filters. Then the filter coefficients can be convolved with the audio input buffer to get the processed audio. Then this process is repeated for each frame.

Calculating filter coefficients might be computationally expensive, but the good news is that we only have to do that when the user changes the filter cutoff. So most of the time the DSP will be free to process audio.
