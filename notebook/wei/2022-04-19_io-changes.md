# GPIO changes

Preset and string indicators should move to newly added encoder switches.

"STR" header is now the PDN output for the DSP. The powerup sequence (see AK7738 full datasheet, page 65) for the DSP is:

1. At least 100 ms after system powerup, set PDN high

2. At least 1 ms later, set control registers.

3. After 10 ms, send the DSP program.

4. 10 ms after, the DSP program starts.
