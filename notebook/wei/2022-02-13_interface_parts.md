Feb 13, 2022

# User interface parts
Tentative list of parts for the user interface hardware.

| Part name | Part no. | Price ($) | Quantity |
| --------- | -------- | --------- | -------- |
| Microcontroller (Kinetis) | MKL43Z128VLH4 | 5.14 | 1 |
| Rotary encoder | ACZ11BR1E-15FD1-12C | 3.58 | 4 |
| Quadrature clock converter | LS7183N-S | 4.89 | 4 |
| Amber LEDs | IN-S42BT5A | 0.32 | 38 | 
| LED resistors (270 ohm) | ERJ-3GEYJ271V | 0.10 | 38 |
| I/O expander with SPI | MCP23S08-E/SO | 1.40 | 4 |
| Crystal oscillator | ABM11W-32.0000MHZ-7-D1X-T3 | 0.662 | 1 |
| Crystal capacitors (C0G) | CC0603DRNPO9BN8R0 | 0.0336| 2 |
| EEPROM | BR25H010FVT-2CE2 | 0.49 | 1 |


# Notes
The LS7183N has delayed shipping from Digikey. If needed, the encoder can work with the microcontroller directly, using interrupt pins. This may increase the computational load on the microcontroller.

The MKL13Z32 is what I would call "good enough". The main requirements are SPI connectivity and MCUXpresso compatibility. The 32K of flash is a potential limitation.

Due to the chip shortage, many other microcontrollers that meet these two requirements are out of stock, which is bad for our 6-week design cycle.

The LED count is an estimate assuming that we design four light bars each with 8 LEDs, plus a power light, four string indicators, and two preset indicators.

The crystal oscillator may be exchanged for any other 32 MHz crystal oscillator, in which case the load capacitors should be updated accordingly. I put in 50 of the capacitors because they tend to get lost during soldering.

The external oscillator of the MKL13Z32 can only be 32 MHz at most. The microcontroller has a high-accuracy 48MHz internal oscillator, which leads me to think that this external oscillator is intended to be configured as a clock source for peripherals rather than the entire microcontroller. We will still include it to be sure.

We mentioned flash memory in the proposal, but EEPROM might be more suited to saving a total of 16 parameters (f0, Q, gain, volume per string). It is more useful to be able to write and rewrite bytes instead of blocks.

The BR25H series EEPROM is chosen because it operates with a 10 MHz SPI clock, which is the same as the I/O expanders.

We may have up to 35 LEDs on at the same time (all 32 lightbar LEDs, plus the string and preset indicators, and a power light; there are four string and two preset indicators, but only one is on at a given time). The updated LEDs are rated at 5 mA current, so they would need 175 mA of current if they were on at the same time. The microcontroller and other digital components will also need their share of the current, so the 3.3V regulator needs to be derated further, possibly to 500 mA.

As of 2022-02-22 the MKL13Z32 is no longer available. A few MKL43Z128s are still available from Digikey. This will also remove the constraints on RAM and program memory we had previously.

# Changelog

### 2022-02-13

- Initial draft
- Added microcontroller, quadrature decoder, rotary encoders, and LEDs/resistors.

### 2022-02-13

- Changed lightbars from 16 LEDs to 8
- Added I/O expander.

### 2022-02-14

- Added crystal oscillator and load caps
-  Added "other" to note about chip shortage 
- I/O expander is no longer tentative
- Changed oscillator from 48 MHz to 32 MHz
- Added EEPROM
- Made changelog all pretty-like
- Updated LED count

### 2022-02-15

- Specified dimmer LEDs
- Moved one LED/resistor to the [power supply parts list](2022-02-14_powersupply_parts.md)

#### 2022-02-22

- Changed microcontroller
