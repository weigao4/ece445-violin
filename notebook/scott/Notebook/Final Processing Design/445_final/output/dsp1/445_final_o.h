// autogenerate ORAM file
#ifndef _AK77XX_DSP1_ORAM_INCLUDED
#define _AK77XX_DSP1_ORAM_INCLUDED


#define _AK77XX_DSP1_ORAM_BUF_SIZE	15

static unsigned char ak77dsp1ORAM[_AK77XX_DSP1_ORAM_BUF_SIZE] = {
	0xB2, 0x00, 0x00,		// command code and address
	0x00, 0x00, 0x07,
	0x00, 0x00, 0x0F,
	0x00, 0x00, 0x17,
	0x00, 0x00, 0x1F
};

#endif		// end of _AK77XX_DSP1_ORAM_INCLUDED
