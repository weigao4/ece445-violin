// autogenerate ORAM file
#ifndef _AK77XX_DSP2_ORAM_INCLUDED
#define _AK77XX_DSP2_ORAM_INCLUDED


#define _AK77XX_DSP2_ORAM_BUF_SIZE	3

static unsigned char ak77dsp2ORAM[_AK77XX_DSP2_ORAM_BUF_SIZE] = {
	0xB3, 0x00, 0x00,		// command code and address

};

#endif		// end of _AK77XX_DSP2_ORAM_INCLUDED
