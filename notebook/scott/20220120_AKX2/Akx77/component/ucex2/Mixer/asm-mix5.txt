﻿;	AsahiKASEI Akx77 - User Component
;
;	<x:ver		>	2.0	
;	<x:type		>	AK77A32
;	<x:ident	>	UserExMixer
;	<x:in		>	5
;	<x:out		>	1
;	<x:cram     >	5

;	<x:ofram    >	-
;	<x:dram0    >	0
;	<x:dram1    >	-
;	<x:dlram    >	-
;	<x:addr.cram     >	-
VOL_IN1_ , 0
VOL_IN2_ , 1
VOL_IN3_ , 2
VOL_IN4_ , 3
VOL_IN5_ , 4

;	<x:code>	body
;[1] Initialize CRAM/DRAM address
       ,LD,$(CRAM[0]):@CP0                                   ; CP0
;[2] Main process starts from here.
       ,LD,$(DRAM[0]):@DP0                                       ; &DRAM in [0]
       ,LD,$(DRAM[1]):@DP0                                       ; &DRAM in [1]
       ,OP,CR*DR,                    ,CPU,   ,   ,       ,      ; <1
       ,LD,$(DRAM[2]):@DP0                                       ; &DRAM in [2]
       ,OP,CR*DR,IDR0=SHL2 <<        ,CPU,   ,   ,       ,      ; <2
       ,LD,$(DRAM[3]):@DP0                                       ; &DRAM in [3]
       ,OP,CR*DR,IDR0=SHL2 + LDR0    ,CPU,   ,   ,       ,      ; <3
       ,LD,$(DRAM[4]):@DP0                                       ; &DRAM in [4]
       ,OP,CR*DR,IDR0=SHL2 + LDR0    ,CPU,   ,   ,       ,      ; <4
       ,OP,     ,                    ,   ,   ,   ,    ,      ;
       ,OP,CR*DR,IDR0=SHL2 + LDR0    ,CPU,   ,   ,       ,      ; <5
       ,OP,     ,                    ,   ,   ,   ,    ,      ;
       ,OP,     ,IDR0=SHL2 + LDR0    ,   ,   ,   ,    ,      ;
;[3] Output: Set the calculation results of the main process to outputs.
       ,OP,     ,                    ,   ,   ,   ,    ,      ;
       ,LD,$(DRAM[5]):@DP0                                       ; DP0 = output
       ,OP,     ,                    ,   ,DU1,   ,FDR0,@DRAM ;
       ,OP,     ,                    ,   ,   ,   ,    ,      ;

