﻿;	AsahiKASEI Akx77 - User Component
;
;	<x:ver		>	2.0	
;	<x:type		>	AK77A32
;	<x:ident	>	UserExDelayPara
;	<x:in		>	5
;	<x:out		>	5
;	<x:cram     >	-

;	<x:ofram    >	10

;	<x:dram0    >	-
;	<x:dram1    >	-
;	<x:dlram    >	$(px[mt1]) + $(px[mt2]) + $(px[mt3]) + $(px[mt4]) + $(px[mt5])
;	<x:addr.oram     >	-
WRITE1_,	 0
WRITE2_,	 1
WRITE3_,	 2
WRITE4_,	 3
WRITE5_,	 4
READ1_, 	 5
READ2_, 	 6
READ3_, 	 7
READ4_, 	 8
READ5_, 	 9

;	<x:code>	body
;[1] Main process starts from here.
       ,LD,$(DRAM[user.in]):@DP0                                       ; DP0 = input 
       ,OP,     ,                    ,   ,DU1,   ,    ,      ;    
       ,PP,     ,IDR0=BH0   <<       ,   ,DU1,   ,DRAM,      ;
       ,PP,     ,IDR1=BH0   <<       ,   ,   ,DLU,DRAM,      ;
       ,PP,     ,IDR2=BH0   <<       ,   ,   ,   ,DRAM,      ;
       ,OP,     ,                    ,   ,DU1,   ,FDR0,@DLYS ; < write
       ,OP,     ,                    ,   ,DU1,DLU,FDR1,@DLYS ; < write
       ,PP,     ,IDR3=BH0   <<       ,   ,   ,DLU,DRAM,      ;
       ,PP,     ,IDR0=BH0   <<       ,   ,   ,DLU,DRAM,      ;
       ,OP,     ,                    ,   ,   ,DLU,FDR2,@DLYS ; < write
       ,OP,     ,                    ,   ,   ,DLU,FDR3,@DLYS ; < write
       ,OP,     ,                    ,   ,   ,DLU,FDR0,@DLYS ; < write
;[2] Output: Set the calculation results of the main process to outputs.
       ,OP,     ,                    ,   ,   ,DLU,    ,      ;
       ,OP,     ,                    ,   ,DU1,DLU,    ,      ;
       ,PP,     ,                    ,   ,DU1,DLU,DLYS,@DRAM ; > out 1 : $(DRAM[out#0])
       ,PP,     ,                    ,   ,DU1,   ,DLYS,@DRAM ; > out 2 : $(DRAM[out#1])
       ,PP,     ,                    ,   ,DU1,   ,DLYS,@DRAM ; > out 3 : $(DRAM[out#2])
       ,PP,     ,                    ,   ,DU1,   ,DLYS,@DRAM ; > out 4 : $(DRAM[out#3])
       ,PP,     ,                    ,   ,DU1,   ,DLYS,@DRAM ; > out 5 : $(DRAM[out#4])
       ,OP,     ,                    ,   ,   ,   ,    ,      ; (wait DRAM)
