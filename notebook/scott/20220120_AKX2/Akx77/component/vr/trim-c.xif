﻿<?xml version="1.0" encoding="utf-8" ?>

<!-- =====================================================
	AsahiKASEI AK77/C - Component Parts
	Trim
========================================================== -->

<xif	xmlns="http://www.asahi-kasei.co.jp/akemd/akx77/xif">

<def	share="Akx Vol"		><![CDATA[

typedef	int	AkxC_TRIM_COEF;			// Vol(Trim) Coef

]]></def>

<!-- ................................................. -->

<coef		cond="o"	><![CDATA[

AkxC_TRIM_COEF	$(sym) = $(px[dB]);			// $(name)
]]></coef>

<mm	cond="o">
	<c	sym="VOL_"	v="0"	/>
</mm>

<!-- ................................................. -->

<proc	share="Akx Vol"	><![CDATA[

//	Compo : Vol(Trim) / Mixer
//..................................................

ae_q56s AkxVr(
			ae_q56s		in,			// input
			AkxC_TRIM_COEF	coef	// coef.
	)
{
	ae_p24x2s	pa, preg;
	ae_q56s		acc, acc2;
	int			tmp;

	in = AE_SATQ48S( in );					// saturation

	pa = AE_MOVPA24( coef );				// pa[H,L] = coef
	tmp = *(int *)&in;						// tmp={least significant 24bit of in}
	preg = AE_MOVPA24( tmp );				// preg[H,L] = tmp
	preg = AE_SRLIP24( preg, 1 );			// logical right shift to make unsigned number

	acc = AE_MULFP24S_LL( preg, pa );		// acc = inL*pa[L]
	acc = AE_SRAIQ56( acc, 21 );			// acc = acc>>21

	preg = AE_TRUNCP24Q48(in);				// preg[H,L] = {upper 24bit of in}
	acc2 = AE_MULFP24S_LL( preg, pa );		// acc2 = preg[L]*pa[L]
	acc2 = AE_SLLIQ56( acc2, 2 );			// acc2 = acc2<<2
	acc = AE_ADDQ56( acc, acc2 );			// acc += acc2 : preg[L]*in<<2

	return (acc);
}

]]></proc>

<!-- ................................................. -->

<LR	><![CDATA[
	$(out) = AkxVr( $(in), $(sym) );		// $(name)
]]></LR>

</xif>
