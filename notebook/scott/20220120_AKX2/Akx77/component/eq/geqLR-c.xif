﻿<?xml version="1.0" encoding="utf-8" ?>

<!-- =====================================================
	AsahiKASEI AK77/C - Component Parts
	GEQ ( IIR2 / Stereo )
========================================================== -->

<xif	xmlns="http://www.asahi-kasei.co.jp/akemd/akx77/xif">

<def	share="Akx EQ/IIR2"	><![CDATA[
typedef	int			Akx_EQ_COEF;		// EQ(IIR2) Coef
typedef	ae_p24x2s	Akx_EQ_DATA;		// EQ(IIR2) Data

]]></def>

<!-- ................................................. -->

<coef	rep="nofBand"	cond="x"><![CDATA[Akx_EQ_COEF	$(sym)_$(rep.ix)[] = $(px[initCoef#rep.ix]);	// GEQ coef
]]></coef>

<data			cond="x"><![CDATA[
Akx_EQ_DATA	$(sym)_Data[$(px[nofBand])][2][4];		// $(name) data

]]></data>

<!-- ................................................. -->

<mm	cond=".">
	<c	sym=""	v="0"	va="_$(rep.ix)"	/>
</mm>

<mm.b	rep="nofBand"	cond="x">
	<ref	href="mm"		/>
</mm.b>

<!-- ................................................. -->
<proc	share="Akx EQ/EQ1SS"		pr="S/S"	><![CDATA[

//	Compo : EQ  mono / Coef.:Single, Data:Single
//..................................................
ae_q56s Akx_IIR2SS(
		ae_q56s		in,				// input
		Akx_EQ_COEF	* cptr,			// coef.
		Akx_EQ_DATA	* dptr			// data(delay)
	)
{
	ae_p24x2s * ptr;
	ae_p24x2s data;
	ae_q56s acc;

	ptr  = (ae_p24x2s *)cptr;
	data = AE_ROUNDSP24Q48SYM(in);							// data.H = data.L = {rounded upper 24bit of in}

	acc = AE_MULZAAFP24S_HH_LL( dptr[0], ptr[0] );			// acc=a2*x02H+a1*x01H
	AE_MULAAFP24S_HH_LL( acc, dptr[1], ptr[1] );			// acc+=b2*x12H+b1*x11H
	AE_MULAAFP24S_HH_LL( acc, data, ptr[2] );				// acc+=a0*inH

	dptr[0] = AE_SELP24_LH( dptr[0], data );				// x02H=x01H; x01H=inH;

	acc = AE_SLLAQ56( acc, cptr[6] );						// acc<<cptr[6]

	data = AE_ROUNDSP24Q48SYM( acc );						// data.H = data.L = {rounded upper 24bit of acc}
	dptr[1] = AE_SELP24_LH( dptr[1], data );				// x12H=x11H; x11H=outH;

	acc = AE_SATQ48S(acc);									// saturation
	return (acc);
}

]]></proc>
<proc	share="Akx EQ/EQ1DS"		pr="S/D"	><![CDATA[

//	Compo : EQ mono / Coef.:Double, Data:Single
//..................................................
ae_q56s Akx_IIR2DS(
		ae_q56s		in,				// input
		Akx_EQ_COEF	* cptr,			// coef.
		Akx_EQ_DATA	* dptr			// data(delay)
	)
{
	ae_p24x2s * ptr;
	ae_p24x2s data;
	ae_q56s acc, acc2;
	int tmp;

	ptr  = (ae_p24x2s *)cptr;
	data = AE_ROUNDSP24Q48SYM(in);							// data.H = data.L = {rounded upper 24bit of in}

	acc = AE_MULZAAFP24S_HH_LL( dptr[0], ptr[0] );			// acc=a2H*x02H+a1H*x01H
	AE_MULAAFP24S_HH_LL( acc, dptr[1], ptr[1] );			// acc+=b2H*x12H+b1H*x11H
	AE_MULAAFP24S_HH_LL( acc, data, ptr[2] );				// acc+=a0H*inH

	acc2 = AE_MULZAAFP24S_HH_LL( dptr[0], ptr[3] );			// acc2=a2L*x02H+a1L*x01H
	AE_MULAAFP24S_HH_LL( acc2, dptr[1], ptr[4] );			// acc2+=b2L*x12H+b1L*x11H
	AE_MULAAFP24S_HH_LL( acc2, data, ptr[5] );				// acc2+=a0L*inH

	dptr[0] = AE_SELP24_LH( dptr[0], data );				// x02H=x01H; x01H=inH

	acc2 = AE_SRAIQ56( acc2, 23 );							// acc2=acc2>>23
	acc = AE_ADDQ56( acc, acc2 );							// acc+=acc2
	acc = AE_SLLAQ56( acc, cptr[12] );						// acc<<cptr[12]

	data = AE_ROUNDSP24Q48SYM( acc );						// data.H = data.L = {rounded upper 24bit of acc}
	dptr[1] = AE_SELP24_LH( dptr[1], data );				// x12H=x11H; x11H=outH;

	acc = AE_SATQ48S(acc);									// saturation
	return (acc);
}

]]></proc>

<!-- ................................................. -->

<proc	share="Akx EQ/EQ1SD"		pr="D/S"	><![CDATA[

//	Compo : EQ  mono / Coef.:Single, Data:Double
//..................................................
ae_q56s Akx_IIR2SD(
		ae_q56s		in,				// input
		Akx_EQ_COEF	* cptr,			// coef.
		Akx_EQ_DATA	* dptr			// data(delay)
	)
{
	ae_p24x2s * ptr;
	ae_p24x2s data;
	ae_q56s acc, acc2;
	int tmp;

	in = AE_SATQ48S( in );									// saturation

	ptr  = (ae_p24x2s *)cptr;
	data = AE_TRUNCP24Q48(in);								// data = {upper 24bit of in}

	acc = AE_MULZAAFP24S_HH_LL( dptr[0], ptr[0] );			// acc=a2*x02H+a1*x01H
	AE_MULAAFP24S_HH_LL( acc, dptr[2], ptr[1] );			// acc+=b2*x12H+b1*x11H
	AE_MULAAFP24S_HH_LL( acc, data, ptr[2] );				// acc+=a0*inH

	dptr[0] = AE_SELP24_LH( dptr[0], data );				// x02H=x01H; x01H=inH;

	tmp = *(int *)&in;										// tmp={least significant 24bit of in}
	data = AE_MOVPA24( tmp );
	data = AE_SRLIP24( data, 1 );							// logical right shift to make unsigned number

	acc2 = AE_MULZAAFP24S_HH_LL( dptr[1], ptr[0] );			// acc2=a2*x02L+a1*x01L
	AE_MULAAFP24S_HH_LL( acc2, dptr[3], ptr[1] );			// acc2+=b2*x12L+b1*x11L
	AE_MULAAFP24S_HH_LL( acc2, data, ptr[2] );				// acc2+=a0*inL

	dptr[1] = AE_SELP24_LH( dptr[1], data );				// x02L=x01L; x01L=inL

	acc2 = AE_SRAIQ56( acc2, 23 );							// acc2=acc2>>23
	acc = AE_ADDQ56( acc, acc2 );							// acc+=acc2
	acc = AE_SLLAQ56( acc, cptr[6] );						// acc<<cptr[6]

	acc = AE_SATQ48S(acc);									// saturation
	data = AE_TRUNCP24Q48( acc );							// data={upper 24bit of acc}
	dptr[2] = AE_SELP24_LH( dptr[2], data );				// x12H=x11H; x11H=outH;

	tmp = *(int *)&acc;										// tmp={least significant 24bit of acc};
	data = AE_MOVPA24( tmp );
	data = AE_SRLIP24( data, 1 );							// logical right shift to make unsigned number
	dptr[3] = AE_SELP24_LH( dptr[3], data );				// x12L=x11L; x11L=outL;

	return (acc);
}

]]></proc>

<proc	share="Akx EQ/EQ1DD"		pr="D/D"	><![CDATA[

//	Compo : EQ mono / Coef.:Double, Data:Double
//..................................................
ae_q56s Akx_IIR2DD(
		ae_q56s		in,				// input
		Akx_EQ_COEF	* cptr,			// coef.
		Akx_EQ_DATA	* dptr			// data(delay)
	)
{
	ae_p24x2s * ptr;
	ae_p24x2s data;
	ae_q56s acc, acc2;
	int tmp;

	in = AE_SATQ48S( in );									// saturation

	ptr  = (ae_p24x2s *)cptr;
	data = AE_TRUNCP24Q48(in);								// data = {upper 24bit of in}

	acc = AE_MULZAAFP24S_HH_LL( dptr[0], ptr[0] );			// acc=a2H*x02H+a1H*x01H
	AE_MULAAFP24S_HH_LL( acc, dptr[2], ptr[1] );			// acc+=b2H*x12H+b1H*x11H
	AE_MULAAFP24S_HH_LL( acc, data, ptr[2] );				// acc+=a0H*inH

	acc2 = AE_MULZAAFP24S_HH_LL( dptr[0], ptr[3] );			// acc2=a2L*x02H+a1L*x01H
	AE_MULAAFP24S_HH_LL( acc2, dptr[2], ptr[4] );			// acc2+=b2L*x12H+b1L*x11H
	AE_MULAAFP24S_HH_LL( acc2, data, ptr[5] );				// acc2+=a0L*inH

	dptr[0] = AE_SELP24_LH( dptr[0], data );				// x02H=x01H; x01H=inH

	tmp = *(int *)&in;										// tmp={least significant 24bit of in}
	data = AE_MOVPA24( tmp );
	data = AE_SRLIP24( data, 1 );							// logical right shift to make unsigned number

	AE_MULAAFP24S_HH_LL( acc2, dptr[1], ptr[0] );			// acc2+=a2H*x02L+a1H*x01L
	AE_MULAAFP24S_HH_LL( acc2, dptr[3], ptr[1] );			// acc2+=b2H*x12L+b1H*x11L
	AE_MULAAFP24S_HH_LL( acc2, data, ptr[2] );				// acc2+=a0H*inL

	dptr[1] = AE_SELP24_LH( dptr[1], data );				// x02L=x01L; x01L=inL

	acc2 = AE_SRAIQ56( acc2, 23 );							// acc2=acc2>>23
	acc = AE_ADDQ56( acc, acc2 );							// acc+=acc2
	acc = AE_SLLAQ56( acc, cptr[12] );						// acc<<cptr[12]

	acc = AE_SATQ48S(acc);									// saturation
	data = AE_TRUNCP24Q48( acc );							// data={upper 24bit of acc}
	dptr[2] = AE_SELP24_LH( dptr[2], data );				// x12H=x11H; x11H=outH;

	tmp = *(int *)&acc;										// tmp={least significant 24bit of acc}
	data = AE_MOVPA24( tmp );
	data = AE_SRLIP24( data, 1 );							// logical right shift to make unsigned number
	dptr[3] = AE_SELP24_LH( dptr[3], data );				// x12L=x11L; x11L=outL;

	return (acc);
}

]]></proc>

<!-- ................................................. -->

<LR				cond="i"	><![CDATA[
	$(acc) = $(in);				// $(name) - $(inIx)
]]></LR>

<LR				cond="x"	><![CDATA[
	{		// $(name)
		ae_q56s		q1 = $(in#0);			// $(name) L in	
		ae_q56s		q2 = $(in#1);			// $(name) R in
]]></LR>

<LR		rep="nofBand"	cond="x"	><![CDATA[
		q1 = Akx_IIR2$(rep.prCD)( q1, $(sym)_$(rep.ix), $(sym)_Data[$(rep.ix)][0] );
		q2 = Akx_IIR2$(rep.prCD)( q2, $(sym)_$(rep.ix), $(sym)_Data[$(rep.ix)][1] );
]]></LR>

<LR				cond="x"	><![CDATA[
		$(out#0) 	= q1;				// $(name) L out
		$(out#1)	= q2;				// $(name) R out
	}
]]></LR>

</xif>
