﻿;	AsahiKASEI Akx77 - User Component
;
;	<x:ver		>	2.0	
;	<x:type		>	AK77A
;	<x:ident	>	UserCompo
;	<x:name		>	User1-$(i)
;	<x:ui.title	>	User Component
;	<x:ui.text	>	Custom component
;	<x:in		>	0
;	<x:out		>	0

;	<x:cram>	-

;	<x:ofram>	-

;	<x:dram0>	-
;	<x:dram1>	-
;	<x:dlram>	-

;	<x:code>	body
       ,LD,$(DRAM[user.in]):@DP0                                       ; DP0 = input
       ,OP,     ,                    ,   ,   ,   ,    ,      ; (nop)
       ;     ( code start )
       ,LD,$(DRAM[user#0]):@DP0                                       ; DP0 = user DRAM
       ,OP,     ,                    ,   ,   ,   ,    ,      ; (nop)
       ,LD,$(DRAM[user.out]):@DP0                                       ; DP0 = output
       ,OP,     ,                    ,   ,   ,   ,    ,      ; (nop)
