﻿//	AsahiKASEI Akx77 - User Component
//
//	<x:ver			>	2.0	
//	<x:type			>	AK77C
//
//	<x:ident		>	UserCompo
//	<x:name			>	User1-$(i)
//
//	<x:ui.title		>	User Component
//	<x:ui.text		>	Custom component
//
//	<x:in			>	0
//	<x:out			>	0

//	<x:pre>		*1 - for Pre Compile Check

#include <stdio.h>
#include <xtensa/hal.h>
#include <xtensa/config/core.h>
#include <xtensa/xtruntime.h>
#include <xtensa/tie/xt_ioports.h>
#include <xtensa/tie/xt_hifi2.h>

//	<x:c>	def

//	<x:c>	coef
int		AkxSym = 0;			// AkxName

//	<x:c>	data
ae_q56s	AkxSym_Data;		// AkxName

//	<x:c>	proc

//	User Component Procedure (shared in the same type)
void AkxCompo_Proc(
		ae_q56s		* in,		// input[]
		ae_q56s		* out,		// output[]
		int			* coef,		// coef[]
		ae_q56s		* data		// data[]
	)
{
}

//	<x:pre>	*1
void SamplingFrame( ae_q56s *AkxIn, ae_q56s *AkxOut )
{

	//	sampling frame procedure call
	//	<x:c>	LR
	AkxCompo_Proc( AkxIn, AkxOut, &AkxSym, &AkxSym_Data );	// AkxName

//	<x:pre>	*1
}
