//	AsahiKASEI AK7707 DSP1
//	

#include <stdio.h>
#include <xtensa/hal.h>
#include <xtensa/config/core.h>
#include <xtensa/xtruntime.h>
#include <xtensa/tie/xt_ioports.h>
#include <xtensa/tie/xt_hifi2.h>
#include <sys/times.h>

typedef volatile int		AkxIoUpdate;	// I/O
typedef volatile long long	AkxIoMem;	// Memory Mapped I/O

#define	true	(1)
#define	false	(0)

// AK7707 AUDIO_IO_XLMI_Address

#define	AKX_AUDIO_IO_BASE	(0x60000000)
#define AKX_AUDIO_IN(n,m) 	(AKX_AUDIO_IO_BASE+(n<<8)+(m<<3))
#define AKX_AUDIO_OUT(n,m)  	(AKX_AUDIO_IO_BASE+0x1000+(n<<8)+(m<<3))

#define	AKX_AUDIO_BUFSIZE	(32)

//	MSBOX I/F

#define AKX_MSBOX1		(0x60010000)
#define AKX_MSBOX2		(0x60011000)

#define AKX_MSBOX1_SIZE		(256)	// 2048byte = 256 long word(64bit x 256)
#define AKX_MSBOX2_SIZE		(256)	// 2048byte = 256 long word(64bit x 256)

#define AKX_RXCMD_MEMCLEAR		(0x00)
#define AKX_RXCMD_WRITE			(0x01)
#define AKX_RXCMD_READ			(0x02)
#define AKX_RXCMD_RAMMONITOR	(0x03)
#define AKX_RXCMD_MCS			(0x04)

//	variables
//........................................

//	I/O Buffer
AkxIoUpdate	AkxAudioIoUpdate1;
AkxIoMem 	AkxAudioIn [8][ AKX_AUDIO_BUFSIZE ];
AkxIoMem 	AkxAudioOut[8][ AKX_AUDIO_BUFSIZE ];

//	MSBOX Control

AkxIoUpdate	AkxMsBoxRxUpdate;
AkxIoMem	AkxMsBoxRxBuf[AKX_MSBOX1_SIZE];

//	variables (coef)
//........................................

//	variables (data)
//........................................

//	functions
//........................................

void AkxMsBoxRxInt()   // GPI01
{
	_xtos_ints_off( XCHAL_EXTINT1_MASK );
	AkxMsBoxRxUpdate = true;
}

void AkxMsBoxRxRead()
{
	int	i;
	AkxIoMem *msbox = (AkxIoMem *)AKX_MSBOX1;
	for ( i=0; i < AKX_MSBOX1_SIZE; i++ ) {
		AkxMsBoxRxBuf[i] = *(msbox+i);
	}
}

//	input buffer int

void AkxAudioIoCtrl1()
{
	int	i,j;
	_xtos_ints_off( XCHAL_EXTINT9_MASK);

	for ( i=0; i < AKX_AUDIO_BUFSIZE; i++ ) {
		for( j=0; j<8; j++ ){
			AkxIoMem	*din  = (AkxIoMem*)AKX_AUDIO_IN(j,i);
			AkxAudioIn[j][i] = *din;
			AkxIoMem	*dout = (AkxIoMem*)AKX_AUDIO_OUT(j,i);
			*dout = AkxAudioOut[j][i];
		}
	}
	AkxAudioIoUpdate1 = true;
	_xtos_ints_on( XCHAL_EXTINT9_MASK);
}

void AkxInitializeInterrupt()
{
	_xtos_set_interrupt_handler( XCHAL_EXTINT1_NUM,		AkxMsBoxRxInt	);

	_xtos_set_interrupt_handler( XCHAL_EXTINT9_NUM,		AkxAudioIoCtrl1 );

	_xtos_ints_on( XCHAL_EXTINT1_MASK	);

	_xtos_ints_on( XCHAL_EXTINT9_MASK	);
}

//	L/R frame
//........................................

void Akx_LR_frame1( int i, int *L, int *R, int *Lo, int *Ro ){

}

//	main
//........................................
int main()
{

	//	I/O Init
	AkxAudioIoUpdate1 = false;

	//	MsBox Int Init
	AkxMsBoxRxUpdate = false;

	AkxInitializeInterrupt();	// int start

	while ( true )
	{

		//	I/O Frame Process

		if( AkxAudioIoUpdate1 ){
			int	i,j;
			for( i=0; i < AKX_AUDIO_BUFSIZE; i++ ){
				int	Li[8], Ri[8];
				int	Lo[8], Ro[8];
				for( j=0; j<8; j++ ){
					long long LR = AkxAudioIn[j][i];
					Li[j] = (long)(LR >> 32);
					Ri[j] = (long)(LR & (long long)0xFFFFFFFF);
				}

				Akx_LR_frame1( i, Li, Ri, Lo, Ro );

				for( j=0; j<8; j++ ){
					AkxAudioOut[j][i] = (   (long long)(Lo[j] & 0xFFFFFFFF) << 32) |
								(long long)(Ro[j] & 0xFFFFFFFF);
				}
			}
			AkxAudioIoUpdate1 = false;
		}

		if( AkxMsBoxRxUpdate ){  // If GPI01 ON read message from PC
			long *rx;
			long cmd, addr, size, sign;
			int  i;

			AkxMsBoxRxUpdate = false;
			AkxMsBoxRxRead();  // Read from MSBOX1 to InBuf[m]

			// MSBOX1 Process
			rx = (long *)AkxMsBoxRxBuf;
			cmd	 = rx[1];

			if( cmd == AKX_RXCMD_WRITE ){
				long *param, *dt;
				long ix = 0;
				addr  = rx[0];		// address
				size  = rx[3];		// size
				param = (long*)addr;

				// Update Parameter
				if( size > 0 ){					// 1st
				 	*param = rx[2];
				}

				dt = &rx[4];
				for( i = 1; i < size; i++ ){	// 2 or later
					if( i & 0x1 ){
						*(param + i) = dt[ix + 1];
					}else{
						*(param + i) = dt[ix];
						ix += 2;
					}
				}

			}else
			if( cmd == AKX_RXCMD_RAMMONITOR ){
				long *msbox, *ram;
				long maxTx;
				sign  = rx[0];		// signature
				addr  = rx[3];		// address
				size  = rx[2];		// size
				msbox = (long*)AKX_MSBOX2;
				ram = (long*)addr;

				maxTx = AKX_MSBOX2_SIZE * 2 - 4; // 4 (use for the acknowledge)
				if( size > maxTx ){
					size -= (size - maxTx);
				}

				msbox[0] = cmd;
				msbox[1] = sign;
				msbox[2] = addr;
				msbox[3] = size;
				for( i = 0; i < size; i++ ){
					*(msbox + 4 + i) = ram[i];  // TX RAM Data
				}

			}
			_xtos_ints_on( XCHAL_EXTINT1_MASK );
		}

	}
	return 0;
}
