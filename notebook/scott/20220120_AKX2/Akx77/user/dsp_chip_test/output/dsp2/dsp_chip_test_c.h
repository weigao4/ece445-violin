// autogenerate CRAM file
#ifndef _AK77XX_DSP2_CRAM_INCLUDED
#define _AK77XX_DSP2_CRAM_INCLUDED


#define _AK77XX_DSP2_CRAM_BUF_SIZE	3

static unsigned char ak77dsp2CRAM[_AK77XX_DSP2_CRAM_BUF_SIZE] = {
	0xB5, 0x00, 0x00,		// command code and address

};

#endif		// end of _AK77XX_DSP2_CRAM_INCLUDED
