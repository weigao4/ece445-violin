# Alex Seong Work Log

# 2021-02-08 - First Meeting with TA Jeff

We were given basic instructions on how to make the design document. Regarding the block diagram, we were asked to revise the connection between the subsystems. Additionally, we were also told to explicitly put the R&V table instead of writing them down as a paragraph. Other than that, we recieved our locker password and regular meeting schedules.

# 2021-02-15 - Meeting about chip selection and platforms

I was assigned to implement the user interface subsystem. Through discussions, we decided to us MKL43Z128VLH4 as our microchip for the user interface and BR25H010-2C as our memory for fulfilling one of the high level requirement of implementing a preset load/save feature. Wei recommanded to use to NXP Xpresso platform as the compiler for the project. The reason is that NXP Xpresso provides direct references to this chip. The following is the data sheet of the user interface micro chip, the memory, and the API references of NXP Xpresso platform.

[MKL43Z128VLH4](https://octopart.com/datasheet/mkl43z128vlh4-nxp+semiconductors-70306571)

[BR25H010-2C](https://pdf1.alldatasheet.com/datasheet-pdf/view/863440/ROHM/BR25H010-2C.html)

[NXP Xpresso](https://mcuxpresso.nxp.com/api_doc/dev/116/index.html)

# 2021-02-22 - First Meeting with Professor Schuh about design document

We did a short presentation about our design document and were given feedbacks. The main feedback was to put the R&V table in a bullet point form rather than writing them with full sentences. We though that this was a reasonable idea for the sake of visibility. Some questions that I got regarding the user interface is the pros and cons of using a rotary encoder to potentiometer that we used in the soldering assignment. We decided to use the rotary encoder since we needed a relative increment and decrement of values in calculating the current parameters. Wei and Scott started focusing on designing the PCB while I was more focused on learning the programming and API calls regarding the SPI protocol and setting options within the NXP Xpresso platform.

![](user_interface_diagram.PNG)

This is the first sketch of our project. We still have some ongoing discussion regarding reducing the usage of buttons but we aim to follow this design.

# 2021-03-01 - Design Review meeting and hardware arrival

We did our presentation on Design Review with Professor Schuh and most of the part was going smoothly. The microchips and peripherals arrived and we were still working on our PCB design. After discussion, we decided to use the mBed platform instead of the NXP Xpresso. The reason is that mbed also provide compatibility with our chip and is more flexible in terms of programming that NXP Xpresso. I was having a hard time configuring the addresses and check if the code is compiling within the NXP Xpresso. Furthermore, mbed provided online compiler which was light in memory and portable compared to the previous platform.

We did a discussion on whether to buy a Dev board, FRDM-KL43Z, to test the user interface program directly. The reason is that PCBs are arriving far in the future, and we wanted to assure if the program that we are going to implement is actually working or not. There are potential problems with testing with our own PCB such as poor soldering. This will make it hard for us to debug which part of the project is not working correctly. Unfortunately, FRDM-KL43Z board is currently out of stock in every online shops and we decided to work on implementing the code first.

The following is the data sheet of FRDM-KL43Z and the mbed API references that we searched.

[FRDM-KL43Z](https://www.digikey.at/htmldatasheets/production/1657838/0/0/1/frdm-kl43z-user-s-guide.html)

[FRDM-KL43Z](https://os.mbed.com/platforms/FRDM-KL43Z/)

[mbed API References](https://os.mbed.com/docs/mbed-os/v6.15/apis/index.html)

Before the first PCB order, we were told from Jeff that our design exceeds the dimension restriction made by the professors. Jeff was helpful in that if we need to integrate both microchips (user interace and the DSP) for performance, he can tell the professor about it. We decided to review our design so that it fits the required dimension restriction before the PCB order.

# 2021-03-06 - IO expander and light bar implementation

I completed the IO expander mapping and the light bars. We used MCP23S08 as our io expander and it communicates to the light bars through SPI. Before having doiscussion with the team, I used the SPI drivers installed within the mbed online compiler but we needed a explicit SPI driver to function correctly in our hardware that will be created in the future.

The following is the data sheet of MCP23S08.

[MCP23S08](https://www.digikey.com/en/htmldatasheets/production/48622/0/0/1/mcp23008-e-p.html?site=US&lang=en&cur=USD&utm_adgroup=Integrated%20Circuits&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Product&utm_term=&utm_content=Integrated%20Circuits&gclid=CjwKCAjw682TBhATEiwA9crl37EMde9YGNGzaiie0aMpUqOGuUFgRjKZjK-XP0C0xh1Ba0zdwp11mRoC0e4QAvD_BwE)

# 2021-03-28 - Meeting after the Spring Break/ Rotary Encoder implementation completed

After the Spring Break, we started working on soldering the PCB that arrived and started testing the requirements for the user interface. Before mounting the program to the PCB, our team had problem that the code that I provided cause compiling error on other computers. The error is "Compiler generates FPU instructions for a device without an FPU (check __FPU_PRESENT)." After some research, we concluded that we can solve this error by defining the FPU_PRESENT in the header file of the pre exisiting librabr. Following is the website that we took reference to solve the issue.

[FPU_PRESENT](https://community.st.com/s/question/0D53W00000BL3mXSAT/error-compiler-generates-fpu-instructions-for-a-device-without-an-fpu-check-fpupresent)

I also completed the rotary encoder implementation and we decided to test these out during the weekend.

# 2021-04-05 - Major issues within the program and bug log

We actually started testing the peripherals out and there were several issues with conneting the program with the board.

The major issue with our program is that the SPI functions are currently not working correctly. This was due to wrong pin assignments in the PCB schematic. Specifically, pins such as PTE29 did not exist in the avaible pin pool of the hardware SPI mapping. Unfortunately, we found out this issue just after ordering our second PCB which means that the same pin assignment was used on the sceond PCB  and the problem will persist. Wei came up with the idea of bypassing this by the help of the software SPI (SWSPI). By using SWSPI, we can manually assign the pin address however we want based on our schematic. The following is the description of the SWSPI.

[SWSPI](https://os.mbed.com/users/davervw/code/SWSPI/)

The disadvantage of using SWSPI is that it is definitely slower than utilizing the pin assignments of the hardware SPI. However, ordering another round of PCB with correct pin assignment will take more time and we thought that this was the best option that we could use. This gave us the work to assign the whole pin address in another syntax, specificall from arduino pin style to addresses that we defined within the schematics. We will probably work on this through the week and hopefully monitor if there is progress.

# 2021-04-09 - Major issues within the program and bug log

With the help of SWSPI, we sucessfully implemented the SPI calls. light bars were lighting up correctly, but now we have an issue of the rotary encoder not reflecting the light bars correctly. Through discussion, we concluded that we have to change the way we keep record of the pulses generated from the rotary encoder. Currently, we are using the polling method which checks if there is pulses generated every loop. The problem is that sometimes, these pulses get skipped due to issues that we are not aware of. In order to prevent this, I started working on changing the polling method to interrupt method which is to explicilty handle the interrupt of incoming pulses and ensure these signals get handled. 

Also, I started working on implementing the EEPROM with preset features. To keep track of how the variables are stored in the memory, I created an excel sheet that maps the addresses of the EEPROM based on the data sheet that is above. Based on the diagram below from the data sheet,

![](datasheet.PNG)

I created the following map address which fits to our need.

![](eeprom_mapping.PNG)

From the diagram, we can see that on each call of the write function, a total of 16 bytes, just like mentioned on the datasheet is written to the memory. With the help of this map, it was much easier to keep track of which address I have to write the values to.

# 2021-04-19 - EEPROM implementation and connection with DSP

Before the mock demo, we discussed about changes and logs that were made within our project.

The first point was that we had to change the way we keep track of the incoming signal from rotary encoder. I was not aware that our rotary encoder is an incrementing encoder which does not sends the signal with the absolute position. Therefore, in the main user interface program, the program was sending direct signal from the encoder to the EEPROM which was not appropriate in terms of the parameters we wanted. In order to solve this issue, Wei implemented an internal state data structure which keeps track of the abolute position of the current parameter and whenever the encoder sends a signal, values stored in this variable changes and reflect these values to the EEPROM.

*Log from 2021-04-29

For our final presentation preparation, I made a simple block diagram denoting the changes we made to how the data is stored to the EEPROM. Since this is relavent to the problem that we encountered on this time of the log, I will add the diagram in this section.

![](block_diagram.png)



The second point was that Scott figured out there is a significant malfunctioning to the software generater of the DSP algorithm that is provided by the DSP chip that we use. Some of the register values were getting skipped and Scott was contacting the engineers to verify the issue. Meanwhile, we concluded that we have to show that the communication between the user interface and the DSP is working correctly. After the meeting, I started studying the datasheet of the DSP chip that we are using. The following is the datasheet of AK7738VQ.

[AK7738VQ](https://www.verical.com/datasheet/akm-semiconductor-dsp-ak7738vq-5034184.pdf)

# 2021-04-26 - Demo Week

Unfortunately, despite contacting the engieers, we were not able to fix the DSP algorithm. According to Scott, there are two solutions to the problem which is to either use another DSP chip or to manually debug every registers that were generated by the software. Both options were nearly impossible given the time and we completed our demo with the verification of the power supply and the user interface.

In conclusion, it was a great experience to create something from scratch where in previous course, a basic structure of the code was given. I took a lot of reference from the ECE385 contents and it really helped me in implementing the user interface subsystem.











