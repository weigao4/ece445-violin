#include "EEPROM.h"
#include "SWSPI.h"


EEPROM::EEPROM()
{
    _spiHandle = NULL;
    _cs = NULL;
    _wp = NULL;
    _hold = NULL;
}


void EEPROM::begin(SWSPI * spi, DigitalOut * cs, DigitalOut * wp, DigitalOut * hold)
{
    statusRegister_t status;
    
    _spiHandle = spi;
    _cs = cs;
    _wp = wp;
    _hold = hold;
    
    *_cs = 1;       // set chip high (disabled state)
    *_wp = 1;       // set wp high (disabled state)
    *_hold = 1;     // set hold high (disabled state)
    
    sendCommand(EEPROM_CMD_WREN);       // send write enable command
    readStatusRegister();

    _statusRegister.value = 0x00;
    writeStatusRegister(_statusRegister);

    // write4Bytes(0, (uint8_t *)&test_write);
    // read4Bytes(0, (uint8_t *)&test_read);
    // while(1);
}

void EEPROM::write4Bytes(const uint8_t addr, const uint8_t * buffer)
{
    readStatusRegister();

    while (_statusRegister.value & 0x01) {
        readStatusRegister();
        ThisThread::sleep_for(1ms);
    }
    sendCommand(EEPROM_CMD_WREN); // Need to write enable before each write operation
    sendWriteCommand(EEPROM_CMD_WRITE, addr, buffer, 4);
    
}

void EEPROM::read4Bytes(const uint8_t addr, uint8_t * value)
{
    readStatusRegister();

    while (_statusRegister.value & 0x01) {
        readStatusRegister();
        ThisThread::sleep_for(1ms);
    }
    
    sendReadCommand(EEPROM_CMD_READ, addr, value, 4);
    
}



void EEPROM::sendCommand(const uint8_t cmd)
{
    *_cs = 0;
    _spiHandle->write(cmd);          
    *_cs = 1;
    ThisThread::sleep_for(1ms);
}

void EEPROM::sendWriteCommand(const uint8_t cmd, const uint8_t addr, const uint8_t * buffer, const uint8_t noOfBytes)
{
    uint8_t i = 0;
    
    *_cs = 0;                                    // cs low
    _spiHandle->write(cmd);                     // send command opcode
    _spiHandle->write(addr);                    // send address
    
    for (i = 0; i < noOfBytes; i++)
    {
        _spiHandle->write(buffer[i]);           // send data values
    }         
    
    *_cs = 1;                                    // cs high
    ThisThread::sleep_for(1ms);
}

void EEPROM::sendReadCommand(const uint8_t cmd, const uint8_t addr, uint8_t * buffer, const uint8_t noOfBytes)
{
    uint8_t i = 0;
    
    *_cs = 0;                                    // cs low
    _spiHandle->write(cmd);                     // send command opcode
    _spiHandle->write(addr);                    // send address
    
    for (i = 0; i < noOfBytes; i++)
    {
        buffer[i] = _spiHandle->write(0x00);    // write dummy value to read data
    }         
    
    *_cs = 1;                                    // cs high
    ThisThread::sleep_for(1ms);
}

void EEPROM::writeStatusRegister(const statusRegister_t status)
{
    *_cs = 0;                                    // cs low
    _spiHandle->write(EEPROM_CMD_WRSR);         // send command opcode
    _spiHandle->write(status.value);   // write dummy value to read data       
    *_cs = 1;                                    // cs high
    ThisThread::sleep_for(1ms);
}

void EEPROM::readStatusRegister(void)
{   
    *_cs = 0;                                            // cs low
    _spiHandle->write(EEPROM_CMD_RDSR);                 // send command opcode
    _statusRegister.value = _spiHandle->write(0xFF);    // write dummy value to read data       
    *_cs = 1;                                            // cs high
    ThisThread::sleep_for(1ms);
}