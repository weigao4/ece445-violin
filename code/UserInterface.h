#ifndef __USER_INTERFACE_H__  
#define __USER_INTERFACE_H__

#include "mbed.h"
#include "LightBar.h"
#include "RotaryEncoder.h"
#include "EEPROM.h"
#include "SWSPI.h"

#define LIGHTBAR_CS                 PTE20
#define LIGHTBAR_VOLUME_ADDR        0
#define LIGHTBAR_GAIN_ADDR          3
#define LIGHTBAR_CF_ADDR            2
#define LIGHTBAR_BW_ADDR            1

#define RENCODER_VOLUME_A           PTD0
#define RENCODER_VOLUME_B           PTD1
#define RENCODER_GAIN_A             PTD2
#define RENCODER_GAIN_B             PTD3
#define RENCODER_CF_A               PTD4
#define RENCODER_CF_B               PTD5
#define RENCODER_BW_A               PTD6
#define RENCODER_BW_B               PTD7

#define EEPROM_CS                   PTE31
#define EEPROM_WP                   PTA1
#define EEPROM_HOLD                 PTA2


#define EEPROM_STRING_SIZE          16
#define EEPROM_NO_OF_STRING         4
#define EEPROM_PRESET_DATA_SIZE     (EEPROM_STRING_SIZE * EEPROM_NO_OF_STRING)

#define EEPROM_ADDR_VOLUME          0x00
#define EEPROM_ADDR_GAIN            0x04
#define EEPROM_ADDR_CF              0x08
#define EEPROM_ADDR_BW              0x0C


#define LED_STRING_1                PTC5
#define LED_STRING_2                PTC4
#define LED_STRING_3                PTC1
#define LED_STRING_4                PTC2

#define LED_PRESET_1                PTC7
#define LED_PRESET_2                PTC6

#define BUTTON_PRESET               PTB16
#define BUTTON_STRING               PTB17

#define ENCODER_TICKS_MAX 100
#define ENCODER_TICKS_MIN 0

#define VOLUME_MIN_VALUE            0.0f
#define VOLUME_MAX_VALUE            100.0f

#define GAIN_MIN_VALUE              0.0f
#define GAIN_MAX_VALUE              3.0f    // 3DB

#define CF_MIN_VALUE                0.0f
#define CF_MAX_VALUE                100.0f

#define BW_MIN_VALUE                0.0f
#define BW_MAX_VALUE                100.0f

typedef enum
{
    STRING_LED_1,
    STRING_LED_2,
    STRING_LED_3,
    STRING_LED_4
}stringLed_e;

typedef enum
{
    PRESET_LED_1,
    PRESET_LED_2,
}presetLed_e;

typedef struct
{
    bool isPressed;
    bool isReleased;
}buttonState_t;

typedef struct
{
    buttonState_t StringLED;
    buttonState_t PresetLED;
}buttons_t;

struct lightbar_states {
     int vol_state;
     int gain_state;
     int bw_state;
     int cf_state;
};

extern DigitalIn pinREncoderVolume_B      ;
extern DigitalIn pinREncoderGain_B        ;
extern DigitalIn pinREncoderCF_B          ;
extern DigitalIn pinREncoderBW_B          ;



class UserInterface
{
    public:
        UserInterface():
             _lBarVolume(LightBar(LIGHTBAR_VOLUME_ADDR)), 
             _lBarGain(LightBar(LIGHTBAR_GAIN_ADDR)),
             _lBarBW(LightBar(LIGHTBAR_BW_ADDR)),
             _lBarCF(LightBar(LIGHTBAR_CF_ADDR)),
             _rEncoderVolume(RENCODER_VOLUME_A),
             _rEncoderGain(RENCODER_GAIN_A),
             _rEncoderBW(RENCODER_BW_A),
             _rEncoderCF(RENCODER_CF_A)
             {}
             
        void begin(SWSPI * spi);
        void runUI(void);
        void updateDisplay();
        
    private:
        LightBar        _lBarVolume;
        LightBar        _lBarGain;
        LightBar        _lBarCF;
        LightBar        _lBarBW;
        
        RotaryEncoder   _rEncoderVolume;
        RotaryEncoder   _rEncoderGain;
        RotaryEncoder   _rEncoderCF;
        RotaryEncoder   _rEncoderBW;
        
        EEPROM          _eeprom;
        
        float           _volume = 0.0f;
        float           _gain = 0.0f;
        float           _cf = 0.0f;
        float           _bw = 0.0f;
        
        stringLed_e     _stringLEDsState = STRING_LED_1;
        presetLed_e     _presetLEDsState = PRESET_LED_1;
        
        buttons_t       _buttons =  {
                                        {.isPressed = false, .isReleased = true},
                                        {.isPressed = false, .isReleased = true}
                                    };

        struct lightbar_states ui_state = {
            0, 0, 0, 0
        };
        
        void calculateActualParameterValues();
        void updateLightBars();
        
        void checkButtonsState();
        void updatePresetLEDsState();
        void updateStringLEDsState();
        
        void loadPresetValues();
        void writePresetValues();

        void updateLightbarStateInternal();
    
};


#endif /* __USER_INTERFACE_H__ */