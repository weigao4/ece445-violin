#ifndef __MCP23S08_H__  
#define __MCP23S08_H__

#include "mbed.h"
#include "SWSPI.h"

#define MCP23S08_ADDR_BASE 0x40

// MCP23S08 Register Address macros
#define MCP23S08_REG_IODIR      0x00
#define MCP23S08_REG_IPOL       0x01
#define MCP23S08_REG_GPINTEN    0x02
#define MCP23S08_REG_DEFVAL     0x03
#define MCP23S08_REG_INTCON     0x04
#define MCP23S08_REG_IOCON      0x05
#define MCP23S08_REG_GPPU       0x06
#define MCP23S08_REG_INTF       0x07
#define MCP23S08_REG_INTCAP     0x08
#define MCP23S08_REG_GPIO       0x09
#define MCP23S08_REG_OLAT       0x0A



class mcp23s08
{
    public:
        mcp23s08(const unsigned int addr);
        void begin(SWSPI * spi, DigitalOut * cs);
        void setIODirection(const unsigned int value);
        unsigned int getIODirection(void);
        void writeIOPort(const unsigned int value);
        unsigned int readIOPort(void);
        
    private:
        SWSPI *           _spiHandle;
        DigitalOut *    _cs;
        unsigned int    _addr;
        
        void writeRegister(const unsigned int regAddr, const unsigned int value);
        unsigned int readRegister(const unsigned int regAddr);
    
};


#endif /* __MCP23S08_H__ */