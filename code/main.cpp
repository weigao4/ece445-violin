/* mbed Microcontroller Library
 * Copyright (c) 2019 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */

#include "ThisThread.h"
#include "mbed.h"
#include "UserInterface.h"
#include <cstdint>
#include "SWSPI.h"
#include "ak7738.h"



// Blinking rate in milliseconds
#define BLINKING_RATE_MS                                                    500
#define UI_UPDATE_RATE_US  1
#define UI_LIGHTBAR_UPDATE_RATE_US 1000

DigitalOut test_led(PTC7);
DigitalIn test_button(PTE0);
DigitalOut test_cs_lights(PTE20);
SWSPI _spi(PTE29, PTE25, PTE30); // mosi, miso, sclk
UserInterface ui;
ak7738 dsp(_spi, PTE21 /* cs */, PTE1 /* jury-rigged PDN */);

Timer timer;


uint64_t micros_since(uint64_t now, uint64_t last) {
    if (now >= last) {
        return now - last;
    } else {
        return (UINT64_MAX - last) + 1 + now; // check if formula is correct?
    }
}

int main()
{

    _spi.format(8, 3);
    _spi.frequency(1000000);

    dsp.init();
   
    test_cs_lights = 1;

    // Dummy write?
    test_cs_lights = 0;
    ThisThread::sleep_for(1ms);
    test_cs_lights = 1;

    int i = 0;
    uint64_t last_ui_update_us;
    uint64_t last_ui_lightbar_update_us = 0;
    uint64_t elapsed;


    ui.begin(&_spi);

    last_ui_update_us = 0;
    timer.start();

    while(1)
    {
        auto now = timer.elapsed_time().count();
        elapsed = micros_since(now, last_ui_update_us);

        if (elapsed > UI_UPDATE_RATE_US) {
            last_ui_update_us = now;
            ui.runUI(); // run task every mSec     
        }

        elapsed = micros_since(now, last_ui_lightbar_update_us);
        if (elapsed > UI_LIGHTBAR_UPDATE_RATE_US) {
            last_ui_lightbar_update_us = now;
            ui.updateDisplay();
        }
        //test_led = test_button;
        
    }
}
