#ifndef __ROTARY_ENCODER_H__  
#define __ROTARY_ENCODER_H__

#include "mbed.h"
#include <cstdint>


#define ENCODER_COUNTER_MAX         INT32_MAX
#define ENCODER_COUNTER_MIN         INT32_MIN
#define ENCODER_COUNTER_STEP        1


class RotaryEncoder
{
    public:
        RotaryEncoder(PinName pinA);
        void begin(DigitalIn * b);
        int32_t getCounterValue();
        void clear_counter();

        
    private:
        InterruptIn     _interruptA;
        DigitalIn *     _b;
        volatile int32_t    _counter = 0;
        unsigned int    _aLastState = 0;
        
        void interruptCallback();
    
};


#endif /* __ROTARY_ENCODER_H__ */