#include "LightBar.h"
#include <math.h>
#include "SWSPI.h"

LightBar::LightBar(const unsigned int addr) : _mcp(mcp23s08(addr))
{
}


void LightBar::begin(SWSPI * spi, DigitalOut * cs)
{
    _mcp.begin(spi, cs);
    _mcp.setIODirection(0x00); // set all IOs as Output
}

void LightBar::setIntensity(const unsigned int value)
{
    _intensity = round(((float)value / 100.0f) * LIGHT_BAR_NO_OF_BITS);
    _mcp.writeIOPort(getPortValue(_intensity));  
}


unsigned int LightBar::getIntensity()
{
    return _intensity;
}


unsigned int LightBar::getPortValue(unsigned int intensity)
{
    switch(intensity)
    {
        case 0:
            return 0x00;
        break;
        
        case 1:
            return 0x01;
        break;
        
        case 2:
            return 0x03;
        break;
        
        case 3:
            return 0x07;
        break;
        
        case 4:
            return 0x0F;
        break;
        
        case 5:
            return 0x1F;
        break;
        
        case 6:
            return 0x3F;
        break;
        
        case 7:
            return 0x7F;
        break;
        
        case 8:
            return 0xFF;
        break;
    }
    
    return 0x00;
}