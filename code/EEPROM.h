#ifndef __EEPROM_H__  
#define __EEPROM_H__

#include <stdint.h>
#include "mbed.h"
#include "SWSPI.h"

#define EEPROM_CMD_WREN             0x06    // Write enable
#define EEPROM_CMD_WRDI             0x04    // write disable
#define EEPROM_CMD_READ             0x03    // read
#define EEPROM_CMD_WRITE            0x02    // write
#define EEPROM_CMD_RDSR             0x05    // read status register
#define EEPROM_CMD_WRSR             0x01    // wtite status register


typedef union
{
    struct
    {
        uint8_t reserved    :4;     // reserved bits
        uint8_t BP1         :1;     // BP1
        uint8_t BP0         :1;     // BP0
        uint8_t WEN         :1;     // write status
        uint8_t R_B         :1;     // read / busy status
    };
    uint8_t value;
}statusRegister_t;


class EEPROM
{
    public:
        EEPROM();
        void begin(SWSPI * spi, DigitalOut * cs, DigitalOut * wp, DigitalOut * hold);
        void write4Bytes(const uint8_t addr, const uint8_t * buffer);
        void read4Bytes(const uint8_t addr, uint8_t * value);
        
    private:
        SWSPI *               _spiHandle;
        DigitalOut *        _cs;
        DigitalOut *        _wp;
        DigitalOut *        _hold;
        
        statusRegister_t    _statusRegister;
        
        void sendCommand(const uint8_t cmd);
        void sendWriteCommand(const uint8_t cmd, const uint8_t addr, const uint8_t * buffer, const uint8_t noOfBytes);
        void sendReadCommand(const uint8_t cmd, const uint8_t addr, uint8_t * buffer, const uint8_t noOfBytes);
        void writeStatusRegister(const statusRegister_t status);
        void readStatusRegister(void);
        
        
    
};


#endif /* __EEPROM_H__ */