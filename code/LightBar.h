#ifndef __LIGHT_BAR_H__  
#define __LIGHT_BAR_H__

#include "mcp23s08.h"
#include "mbed.h"
#include "SWSPI.h"

#define LIGHT_BAR_NO_OF_BITS        8.0f

class LightBar
{
    public:
        LightBar(const unsigned int addr);
        void begin(SWSPI * spi, DigitalOut * cs);
        void setIntensity(const unsigned int value);
        unsigned int getIntensity();
        
        
    private:
        mcp23s08      _mcp;
        unsigned int    _intensity = 0;
        
        unsigned int getPortValue(unsigned int intensity);
};


#endif /* __LIGHT_BAR_H__ */