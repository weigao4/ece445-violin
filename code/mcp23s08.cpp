#include "mcp23s08.h"
#include "SWSPI.h"

mcp23s08::mcp23s08(const unsigned int addr)
{
    _spiHandle = NULL;
    _cs = NULL;
    _addr = addr;
    
}

void mcp23s08::begin(SWSPI * spi, DigitalOut * cs)
{
    _spiHandle = spi;
    _cs = cs;
    
    *_cs = 1;       // set chip high (disabled state)

    this->writeRegister(MCP23S08_REG_IOCON, 0x28); // hardware addresses enabled, SEQOP disabled
}


void mcp23s08::setIODirection(const unsigned int value)
{
    writeRegister(MCP23S08_REG_IODIR, value);
}


unsigned int mcp23s08::getIODirection(void)
{
    return readRegister(MCP23S08_REG_IODIR);
}


void mcp23s08::writeIOPort(const unsigned int value)
{
    writeRegister(MCP23S08_REG_GPIO, value);
}

unsigned int mcp23s08::readIOPort(void)
{
    return readRegister(MCP23S08_REG_GPIO);
}


void mcp23s08::writeRegister(const unsigned int regAddr, const unsigned int value)
{
    unsigned int opcode = 0;
    
    opcode = MCP23S08_ADDR_BASE | (_addr << 1);              // make opcode with R/W bit 0 (WRITE operation)
    
    *_cs = 0;                            // chip select low
    _spiHandle->write(opcode);          // send opcode
    _spiHandle->write(regAddr & 0xFF);  // send register Address
    _spiHandle->write(value);           // send value to be written
    *_cs = 1;                            // chip select high
}

unsigned int mcp23s08::readRegister(const unsigned int regAddr)
{
    unsigned int opcode = 0;
    unsigned int value = 0;
    
    opcode = MCP23S08_ADDR_BASE | (_addr << 1) | 0x01;       // make opcode with R/W bit 1 (READ operation)
    
    *_cs = 0;                            // chip select low
    _spiHandle->write(opcode);          // send opcode
    _spiHandle->write(regAddr & 0xFF);  // send register Address
    value = _spiHandle->write(0xFF);    // send dummy value to be read register value
    *_cs = 1;                            // chip select high
    
    return value;
}


