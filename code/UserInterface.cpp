#include "UserInterface.h"
#include <stdint.h>
#include "SWSPI.h"

// light bar chip select
DigitalOut pinCS_LBAR               (LIGHTBAR_CS, 1);

// EEPROM pins
DigitalOut pinCS_EEPROM             (EEPROM_CS, 1);
DigitalOut pinWP_EEPROM             (EEPROM_WP, 1);
DigitalOut pinHOLD_EEPROM           (EEPROM_HOLD, 1);

// string LED pins
DigitalOut pinLedString1           (LED_STRING_1);
DigitalOut pinLedString2           (LED_STRING_2);
DigitalOut pinLedString3           (LED_STRING_3);
DigitalOut pinLedString4           (LED_STRING_4);

// preset LED pins
DigitalOut pinledPreset1           (LED_PRESET_1);
DigitalOut pinledPreset2           (LED_PRESET_2);

// rotary encoder pins
DigitalIn pinREncoderVolume_A      (RENCODER_VOLUME_A);
DigitalIn pinREncoderVolume_B      (RENCODER_VOLUME_B);
DigitalIn pinREncoderGain_A        (RENCODER_GAIN_A);
DigitalIn pinREncoderGain_B        (RENCODER_GAIN_B);
DigitalIn pinREncoderCF_A          (RENCODER_CF_A);
DigitalIn pinREncoderCF_B          (RENCODER_CF_B);
DigitalIn pinREncoderBW_A          (RENCODER_BW_A);
DigitalIn pinREncoderBW_B          (RENCODER_BW_B);

// button pins
DigitalIn pinButtonPreset       (BUTTON_PRESET);
DigitalIn pinButtonString       (BUTTON_STRING);


void UserInterface::begin(SWSPI * spi)
{
    // initialize all light bars
    _lBarVolume.begin(spi, &pinCS_LBAR);
    _lBarGain.begin(spi, &pinCS_LBAR);
    _lBarCF.begin(spi, &pinCS_LBAR);
    _lBarBW.begin(spi, &pinCS_LBAR);
    
    // set light bar intensities to 0
    _lBarVolume.setIntensity(0);
    _lBarGain.setIntensity(0);
    _lBarCF.setIntensity(0);
    _lBarBW.setIntensity(0);
    
    // // Initialize all Rotary Encoders
    _rEncoderVolume.begin(&pinREncoderVolume_B);
    _rEncoderGain.begin(&pinREncoderGain_B);
    _rEncoderCF.begin(&pinREncoderCF_B);
    _rEncoderBW.begin(&pinREncoderBW_B);

     pinLedString1 = 1;
    pinLedString2 = 0;
    pinLedString3 = 0;
    pinLedString4 = 0;

    pinledPreset1 = 1;
    pinledPreset2 = 0;
    
    // initialize eeprom
    _eeprom.begin(spi, &pinCS_EEPROM, &pinWP_EEPROM, &pinHOLD_EEPROM);
    loadPresetValues();
    

}


void UserInterface::runUI(void)
{
   
    updateLightbarStateInternal();              // Update internal state counters from rotary encoders
    checkButtonsState();                        // Check buttons state
    calculateActualParameterValues();           // Calculate actual parameter values
    
    
    if (_buttons.PresetLED.isPressed)           // preset button pressed
    {
        _buttons.PresetLED.isPressed = false;
        writePresetValues();    // Save current preset
        updatePresetLEDsState();                // update preset leds state & load preset values
        loadPresetValues(); // Load next preset after preset counter updates
        updateLightBars();                          // update light bar intensities
    }
    
    if (_buttons.StringLED.isPressed)           // string button pressed
    {
        _buttons.StringLED.isPressed = false;
        writePresetValues(); // Save current
        updateStringLEDsState();                // update string leds state
        loadPresetValues(); // Load next, after string counter updates
        updateLightBars();                          // update light bar intensities
    }

    
}



void UserInterface::calculateActualParameterValues()
{
    _volume = (VOLUME_MAX_VALUE - VOLUME_MIN_VALUE) * ui_state.vol_state;
    _gain = (GAIN_MAX_VALUE - GAIN_MIN_VALUE) * ui_state.gain_state;
    _cf = (CF_MAX_VALUE - CF_MIN_VALUE) * ui_state.cf_state;
    _bw = (BW_MAX_VALUE - BW_MIN_VALUE) * ui_state.bw_state;
}


void UserInterface::updateLightbarStateInternal() {
    // Poll the rotary encoders to see how many rotation "ticks" were counted since last poll.
    int32_t enc_vol_ticks = _rEncoderVolume.getCounterValue();
    int32_t enc_gain_ticks = _rEncoderGain.getCounterValue();
    int32_t enc_bw_ticks = _rEncoderBW.getCounterValue();
    int32_t enc_cf_ticks = _rEncoderCF.getCounterValue();

    // Update internal UI state using these ticks.
    if (ui_state.vol_state + enc_vol_ticks > ENCODER_TICKS_MAX) {
        ui_state.vol_state = ENCODER_TICKS_MAX;
    } else if (ui_state.vol_state + enc_vol_ticks < ENCODER_TICKS_MIN) {
        ui_state.vol_state = ENCODER_TICKS_MIN;
    } else {
        ui_state.vol_state += enc_vol_ticks;
    }

    if (ui_state.gain_state + enc_gain_ticks > ENCODER_TICKS_MAX) {
        ui_state.gain_state = ENCODER_TICKS_MAX;
    } else if (ui_state.gain_state + enc_gain_ticks < ENCODER_TICKS_MIN) {
        ui_state.gain_state = ENCODER_TICKS_MIN;
    } else {
        ui_state.gain_state += enc_gain_ticks;
    }

    if (ui_state.bw_state + enc_bw_ticks > ENCODER_TICKS_MAX) {
        ui_state.bw_state = ENCODER_TICKS_MAX;
    } else if (ui_state.bw_state + enc_bw_ticks < ENCODER_TICKS_MIN) {
        ui_state.bw_state = ENCODER_TICKS_MIN;
    } else {
        ui_state.bw_state += enc_bw_ticks;
    }

    if (ui_state.cf_state + enc_cf_ticks > ENCODER_TICKS_MAX) {
        ui_state.cf_state = ENCODER_TICKS_MAX;
    } else if (ui_state.cf_state + enc_cf_ticks < ENCODER_TICKS_MIN) {
        ui_state.cf_state = ENCODER_TICKS_MIN;
    } else {
        ui_state.cf_state += enc_cf_ticks;
    }

    // Clear the counters for each encoder so previous rotation ticks don't affect the next poll.
    _rEncoderVolume.clear_counter();
    _rEncoderGain.clear_counter();
    _rEncoderBW.clear_counter();
    _rEncoderCF.clear_counter();
}

void UserInterface::updateLightBars()
{
    _lBarVolume.setIntensity(ui_state.vol_state);
    _lBarGain.setIntensity(ui_state.gain_state);
    _lBarCF.setIntensity(ui_state.cf_state);
    _lBarBW.setIntensity(ui_state.bw_state);
}

void UserInterface::updateDisplay() {
    updateLightBars();                          // update light bar intensities
}


void UserInterface::checkButtonsState(void)
{
    static unsigned int btnPresetPressCount = 0;
    static unsigned int btnStringPressCount = 0;
    
    if (pinButtonPreset == 1 && _buttons.PresetLED.isReleased == true)
    {
        btnPresetPressCount++;
        if (btnPresetPressCount > 10 && pinButtonPreset == 1)
        {
            btnPresetPressCount = 0;
            _buttons.PresetLED.isPressed = true;
            _buttons.PresetLED.isReleased = false;
        }
    }
    else if (pinButtonPreset == 0)
    {
        btnPresetPressCount = 0;
        _buttons.PresetLED.isReleased = true;
    }
    
    if (pinButtonString == 1 && _buttons.StringLED.isReleased == true)
    {
        btnStringPressCount++;
        if (btnStringPressCount > 10 && pinButtonString == 1)
        {
            btnStringPressCount = 0;
            _buttons.StringLED.isPressed = true;
            _buttons.StringLED.isReleased = false;
        }
    }
    else if (pinButtonString == 0)
    {
        btnStringPressCount = 0;
        _buttons.StringLED.isReleased = true;
    }
}

void UserInterface::updatePresetLEDsState()
{
    switch(_presetLEDsState)
    {   
        case PRESET_LED_1:
            _presetLEDsState = PRESET_LED_2;
            pinledPreset1 = 0;
            pinledPreset2 = 1;
        break;
        
        case PRESET_LED_2:
            _presetLEDsState = PRESET_LED_1;
            pinledPreset1 = 1;
            pinledPreset2 = 0;
        break;
    }
    
    loadPresetValues();     // load other preset values of the selected string
}


void UserInterface::updateStringLEDsState()
{
    switch(_stringLEDsState)
    {
        case STRING_LED_1:
            _stringLEDsState = STRING_LED_2;
            pinLedString1 = 0;
            pinLedString2 = 1;
            pinLedString3 = 0;
            pinLedString4 = 0;
        break;
        
        case STRING_LED_2:
            _stringLEDsState = STRING_LED_3;
            pinLedString1 = 0;
            pinLedString2 = 0;
            pinLedString3 = 1;
            pinLedString4 = 0;
        break;
        
        case STRING_LED_3:
            _stringLEDsState = STRING_LED_4;
            pinLedString1 = 0;
            pinLedString2 = 0;
            pinLedString3 = 0;
            pinLedString4 = 1;
        break;
        
        case STRING_LED_4:
            _stringLEDsState = STRING_LED_1;
            pinLedString1 = 1;
            pinLedString2 = 0;
            pinLedString3 = 0;
            pinLedString4 = 0;
        break;
    }
    
    loadPresetValues();         // load preset values for the string
}

void UserInterface::loadPresetValues()
{
    int32_t temp = 0;
    uint8_t offsetAddress = 0;
    
    offsetAddress = (_presetLEDsState * EEPROM_PRESET_DATA_SIZE) + (_stringLEDsState * EEPROM_STRING_SIZE);
    // how about 
    // _presetLEDsState
    
    // load volume data
    _eeprom.read4Bytes(offsetAddress + EEPROM_ADDR_VOLUME, (uint8_t *)&temp);
    if (temp == 0xFFFFFFFF)
    {
        ui_state.vol_state = 0;
        //_eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_VOLUME, (uint8_t *)&((ui_state.vol_state)));
    }
    else
    {
       ui_state.vol_state = temp;
    }
    
    
    // load gain data
    _eeprom.read4Bytes(offsetAddress + EEPROM_ADDR_GAIN, (uint8_t *)&temp);
    if (temp == 0xFFFFFFFF)
    {
        ui_state.gain_state = 0;
        //_eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_GAIN, (uint8_t *)&(ui_state.gain_state));
    }
    else
    {
        ui_state.gain_state = temp;
    }
    
    
    // load CF data
    _eeprom.read4Bytes(offsetAddress + EEPROM_ADDR_CF, (uint8_t *)&temp);
    if (temp == 0xFFFFFFFF)
    {
        ui_state.cf_state  = 0;
        //_eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_CF, (uint8_t *)&(ui_state.cf_state));
    }
    else
    {
        ui_state.cf_state = temp;
    }
    
    
    // load BW data
    _eeprom.read4Bytes(offsetAddress + EEPROM_ADDR_BW, (uint8_t *)&temp);
    if (temp == 0xFFFFFFFF)
    {
        ui_state.bw_state = 0;
        //_eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_BW, (uint8_t *)&(ui_state.bw_state));
    }
    else
    {
        ui_state.bw_state = temp;
    }
}


void UserInterface::writePresetValues()
{
    uint8_t offsetAddress = 0;
    
    offsetAddress = (_presetLEDsState * EEPROM_PRESET_DATA_SIZE) + (_stringLEDsState * EEPROM_STRING_SIZE);

    // TODO: save the int32's from ui_state
    _eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_VOLUME, (uint8_t *)&(ui_state.vol_state));  // write volume data
    _eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_GAIN, (uint8_t *)&(ui_state.gain_state));      // write gain data
    _eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_CF, (uint8_t *)&(ui_state.cf_state));          // write cf data
    _eeprom.write4Bytes(offsetAddress + EEPROM_ADDR_BW, (uint8_t *)&(ui_state.bw_state));          // write bw data
}
