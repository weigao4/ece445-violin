#ifndef AK7738_H
#define AK7738_H

#include "DigitalOut.h"
#include "mbed.h"
#include "SWSPI.h"


class ak7738 {
public:
    ak7738(SWSPI& spi, PinName cs, PinName pdn);
    void init(); // Power up and program

private:
    SWSPI& spi_;
    DigitalOut pdn_;
    DigitalOut cs_;

};

#endif