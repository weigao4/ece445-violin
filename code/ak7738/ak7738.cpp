#include "mbed.h"
#include "ak7738.h"
#include "ak77xx_pram.h"
#include "ak77xx_cram.h"
#include "ak77xx_ofreg.h"

ak7738::ak7738(SWSPI& spi, PinName cs, PinName pdn) : spi_(spi), pdn_(pdn), cs_(cs) {
    pdn_ = 0;
    cs_ = 1;
}

void ak7738::init() {
    // set PDN high, then wait 100 ms
    pdn_ = 1;
    ThisThread::sleep_for(100ms);

    // Write DSP registers
    int i;

    // Write control registers "CONT REG"
    cs_ = 0;
    spi_.write(0xC0);
    spi_.write(0x00);
    spi_.write(0x00);
    spi_.write(0x13); // CLKO Output Setting
    spi_.write(0x00);
    cs_ = 1;

    // Wait 10 ms
    ThisThread::sleep_for(100ms);
    
    // Write OFREG
    // First line in the array is the command to write to that memory bank
    // Everything that follows is to be written to the memory
    cs_ = 0;
    for (i = 0; i < 3; ++i) {
        spi_.write(ak77dspOFREG[i]);
    }
    for (i = 0; i < _AK77XX_DSP_OFREG_WRITE_SIZE; ++i) {
        spi_.write(ak77dspOFREG[i + 3]);
    }

    // Write PRAM
    for (i = 0; i < 3; ++i) {
        spi_.write(ak77dspPRAM[i]);
    }
    for (i = 0; i < _AK77XX_DSP_PRAM_WRITE_SIZE; ++i) {
        spi_.write(ak77dspPRAM[i + 3]);
    }

    // Write CRAM
    for (i = 0; i < 3; ++i) {
        spi_.write(ak77dspCRAM[i]);
    }
    for (i = 0; i < _AK77XX_DSP_CRAM_WRITE_SIZE; ++i) {
        spi_.write(ak77dspCRAM[i + 3]);
    }

    // End programming
    cs_ = 1;
}