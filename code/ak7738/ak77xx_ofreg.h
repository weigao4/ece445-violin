﻿// AK77xx OFREG file (dsp_chip_test.off)
#ifndef _AK77XX_DSP_OFREG_INCLUDED
#define _AK77XX_DSP_OFREG_INCLUDED

#define _AK77XX_DSP_OFREG_WRITE_SIZE	0
static unsigned char ak77dspOFREG[] = {
    0xB2, 0x00, 0x00, // command code and address
};

#endif    // end of _AK77XX_DSP_OFREG_INCLUDED
