#include "RotaryEncoder.h"


RotaryEncoder::RotaryEncoder(PinName pinA) : _interruptA(pinA)
{
    _b = NULL;
    _counter = 0;
}


void RotaryEncoder::begin(DigitalIn * b)
{
    _b = b;
    
    _interruptA.fall(callback(this, &RotaryEncoder::interruptCallback)); // attach callback function to the interrupt
}


int32_t RotaryEncoder::getCounterValue()
{
    return _counter;
}


void RotaryEncoder::interruptCallback()
{
    if (*_b)    // Direction = CW
    {
        _counter += ENCODER_COUNTER_STEP;
    }
    else        // Direction = CCW
    {
        _counter -= ENCODER_COUNTER_STEP;
    }
}

void RotaryEncoder::clear_counter() {
    _counter = 0;
}

